import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'item-sorted', loadChildren: './item-sorted/item-sorted.module#ItemSortedPageModule' },
  { path: 'item-sorted/:id', loadChildren: './item-sorted/item-sorted.module#ItemSortedPageModule' },   { path: 'modal', loadChildren: './modal/modal.module#ModalPageModule' },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
