import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestService } from '../services/rest/rest.service';
import { AdmobService } from '../services/admob/admob.service';
import { Product } from '../models/product/product.model';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-item-sorted',
  templateUrl: './item-sorted.page.html',
  styleUrls: ['./item-sorted.page.scss'],
})
export class ItemSortedPage implements OnInit {

  sliderConfig = {
    spaceBetween: 5,
    centeredSlides: true,
    slidesPerView: 1.2,
    loop: false,
    zoom: false

  }

  alphabet: string;
  private nodata: boolean = false;
  private products : Product[] = [];
  private productsLocal : Product[] = [];
  imageApi = 'http://ec2-13-59-238-145.us-east-2.compute.amazonaws.com:3000/image';

  constructor(private route: ActivatedRoute, private restService: RestService, private admobService: AdmobService, private modalController: ModalController) {
    this.admobService.adCounting();
  }

  ngOnInit() {
    this.alphabet = this.route.snapshot.paramMap.get('id');
    this.getProducts();
    console.log('products');
    console.log(this.products);
    this.admobService.activateAds();
  }

  async presentModal(header, message) {
    const modal = await this.modalController.create({
      component: ModalPage,
      cssClass: 'my-custom-modal-css',
      componentProps: { header: header, message: message }
    });
    return await modal.present();
  }

  getProducts()
  {
    this.restService.getProductsLocal().subscribe((products : Product[])=>{
          this.products = products.filter(product =>  product.tag.toUpperCase() === this.alphabet),
          (err => {
              this.getProductsLocal();
          });
        });
  }

  getProductsLocal()
  {
    this.restService.getProductsLocal().subscribe((products : Product[])=>{
          this.productsLocal = products.filter(product =>  product.tag.toUpperCase() === this.alphabet),
          (err => {
              this.nodata = true;
          });
        });
  }



}
