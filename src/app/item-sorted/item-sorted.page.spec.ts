import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemSortedPage } from './item-sorted.page';

describe('ItemSortedPage', () => {
  let component: ItemSortedPage;
  let fixture: ComponentFixture<ItemSortedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSortedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSortedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
