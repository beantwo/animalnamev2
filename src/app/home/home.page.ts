import { Component, OnInit } from '@angular/core';
import { AdmobService } from '../services/admob/admob.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  abcd = ["A","B","C","D"];
  efgh = ["E", "F", "G", "H"];
  ijkl = ["I", "J", "K", "L"];
  mnop = ["M", "N", "O", "P"];
  qrst = ["Q", "R", "S", "T"];
  uvwx = ["U", "V", "W", "X"];
  yz = ["Y", "Z"];
  alphabets = [this.abcd, this.efgh, this.ijkl, this.mnop, this.qrst, this.uvwx, this.yz];
  subscription: any;

  constructor(private admobService: AdmobService, private platform: Platform) {
    this.admobService.adCounting();
  }

  ngOnInit() {
    this.admobService.activateAds();
  }

  ionViewDidEnter(){
        this.subscription = this.platform.backButton.subscribe(()=>{
            navigator['app'].exitApp();
        });
  }

  ionViewWillLeave(){
        this.subscription.unsubscribe();
  }

}
