export class Product {
   _id: string;
   name: string;
   title: string;
   image: string;
   description: string;
   tag: string;
   habitat: string;
   makanan: string;
   status_konservasi: string;
   constructor(values: Object = {}) {
        Object.assign(this, values);
   }
}
