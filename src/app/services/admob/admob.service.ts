import { Injectable } from '@angular/core';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free/ngx';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AdmobService {

  constructor(private adMobFree: AdMobFree, private storage: Storage) {}

  adCounting() {
    this.storage.get('adCounter').then((val) => {
      if(val) {
        val += 1;
        this.storage.set('adCounter', val);
      } else
      {
        this.storage.set('adCounter', 1);
      }
    });
  }

  activateAds() {
    this.storage.get('adCounter').then((val) => {
      if(val) {
        console.log("adCounter");
        console.log(val);
        if (val > 20)
        {
          this.launchInterstitial();
          val = 0;
          this.storage.set('adCounter', val);
        }
      }
    });
    this.showBannerAd();
  }

  async showBannerAd() {
    try {
      const bannerConfig: AdMobFreeBannerConfig = {
        id: 'ca-app-pub-5721185983606157/2254478118',
        //isTesting: false,
        autoShow: true
      }

      this.adMobFree.banner.config(bannerConfig);

      const result = await this.adMobFree.banner.prepare();
      console.log(result);
    }
    catch (e) {
      console.error(e);
    }
  }

  launchInterstitial() {

      let interstitialConfig: AdMobFreeInterstitialConfig = {
          id: 'ca-app-pub-5721185983606157/9157324006',
          //isTesting: false, // Remove in production
          autoShow: true
          //id: Your Ad Unit ID goes here
      };

      this.adMobFree.interstitial.config(interstitialConfig);

      this.adMobFree.interstitial.prepare().then(() => {
          // success
      });

  }
}
