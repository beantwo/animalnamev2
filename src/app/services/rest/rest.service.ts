import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Product } from '../../models/product/product.model';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  jsonLocal = '../../../assets/json/animals.json';
  jsonApi = 'http://ec2-13-59-238-145.us-east-2.compute.amazonaws.com:3000/api';

  constructor(private http: HttpClient) { }

  getProductsLocal(): Observable<Product[]> {
    return this.http.get<Product[]>(this.jsonLocal)
  }

  getProductsApi(): Observable<Product[]> {
    return this.http.get<Product[]>(this.jsonApi)
  }

}
